﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Button[] btns;
        private int[][] Fields;

        private void NewBoard(int n)
        {
            CheckerBoard.Children.Clear();

            CheckerBoard.Rows = n;
            CheckerBoard.Columns = n;
            Stop = -1;
            Start = -1;

            btns = new Button[n*n];
            Fields = new int[n][];
            for (int j = 0; j < n; j++) Fields[j] = new int[n];

            for (int i = 0; i < n * n; i++)
            {
                btns[i] = new Button();

                btns[i].Style = App.Current.Resources["Hand"] as Style;
                btns[i].Background = EmptyBrush.Clone();

                btns[i].MouseEnter += new MouseEventHandler (Board_Enter);
                btns[i].MouseLeave += new MouseEventHandler (Board_Leave);
                btns[i].Click      += new RoutedEventHandler(Board_Click);

                btns[i].Tag = i;
                Fields[i / n][i % n] = 3000;

                CheckerBoard.Children.Add(btns[i]);
                
            }
        }

        Rectangle WallBrushSquare = new Rectangle() { Width = 12, Height = 12, Stroke = Brushes.Black };
        Rectangle EmptyBrushSquare = new Rectangle() { Width = 12, Height = 12, Stroke = Brushes.Black };
        Rectangle StartBrushSquare = new Rectangle() { Width = 12, Height = 12, Stroke = Brushes.Black };
        Rectangle StopBrushSquare = new Rectangle() { Width = 12, Height = 12, Stroke = Brushes.Black };

        private void NewRadioPanel()
        {
            WallBrushSquare.Fill = WallBrush;
            EmptyBrushSquare.Fill = EmptyBrush;
            StartBrushSquare.Fill = StartBrush;
            StopBrushSquare.Fill = StopBrush;

            WallChoice = new RadioButton() { Style = App.Current.Resources["BigBullet"] as Style };
            StackPanel WallPanel = new StackPanel() { Orientation = Orientation.Horizontal };
                       WallPanel.Children.Add(new TextBlock() { FontSize = 11, Width = 60, Text = "Wall Brush", Margin=new Thickness(5)});
                       WallPanel.Children.Add(WallBrushSquare);
            WallChoice.Content = WallPanel;
            WallChoice.IsChecked = true;

            EmptyChoice = new RadioButton() { Style = App.Current.Resources["BigBullet"] as Style };
            StackPanel EmptyPanel = new StackPanel() { Orientation = Orientation.Horizontal };
                       EmptyPanel.Children.Add(new TextBlock() { FontSize = 11, Width = 60, Text = "Empty Brush", Margin = new Thickness(5) });
                       EmptyPanel.Children.Add(EmptyBrushSquare);
            EmptyChoice.Content = EmptyPanel;
            EmptyChoice.Style = App.Current.Resources["BigBullet"] as Style;

            StartChoice = new RadioButton() { Style = App.Current.Resources["BigBullet"] as Style };
            StackPanel StartPanel = new StackPanel() { Orientation = Orientation.Horizontal };
                       StartPanel.Children.Add(new TextBlock() { FontSize = 11, Width=60, Text = "Start Brush", Margin = new Thickness(5) });
                       StartPanel.Children.Add(StartBrushSquare);
            StartChoice.Content = StartPanel;

            StopChoice = new RadioButton() { Style = App.Current.Resources["BigBullet"] as Style };
            StackPanel StopPanel = new StackPanel() { Orientation = Orientation.Horizontal };
                       StopPanel.Children.Add(new TextBlock() { FontSize = 11, Width = 60, Text = "Stop Brush", Margin = new Thickness(5) });
                       StopPanel.Children.Add(StopBrushSquare);
            StopChoice.Content = StopPanel;

            Radio.Children.Add(WallChoice);
            Radio.Children.Add(EmptyChoice);
            Radio.Children.Add(StartChoice);
            Radio.Children.Add(StopChoice);
        }

        public MainWindow()
        {
            EmptyBrush = new SolidColorBrush(Brushes.Gainsboro.Color);
            WallBrush = new SolidColorBrush(Brushes.Blue.Color);
            PathBrush = new SolidColorBrush(Brushes.PaleTurquoise.Color);
            StartBrush = new SolidColorBrush(Brushes.LawnGreen.Color);
            StopBrush = new SolidColorBrush(Brushes.Red.Color);

            InitializeComponent();

            NewBoard(7);

            NewRadioPanel();

            for (int i = 0; i < 46; i++)
                items[i] = (i + 5) + "x" + (i + 5);

            cmbSize.ItemsSource = items;
            cmbSize.SelectedIndex = 2;            
        }

        string[] items = new string[46];
        
        int Stop = -1;
        int Start = -1;
        
        RadioButton WallChoice;
        RadioButton StartChoice;
        RadioButton StopChoice;
        RadioButton EmptyChoice;
        public SolidColorBrush EmptyBrush;
        public SolidColorBrush WallBrush;
        public SolidColorBrush StartBrush;
        public SolidColorBrush StopBrush;
        public SolidColorBrush PathBrush;

        public void Board_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            int n = CheckerBoard.Rows;

            if ((int)button.Tag == Stop) Stop = -1;
            if ((int)button.Tag == Start) Start = -1;

            if (WallChoice.IsChecked == true)
            {
                button.Background = WallBrush.Clone();
                Fields[(int)button.Tag / n][(int)button.Tag % n] = -1;
            }
            if (EmptyChoice.IsChecked == true)
            {
                button.Background = EmptyBrush.Clone();
                Fields[(int)button.Tag / n][(int)button.Tag % n] = 3000;
            }
            if (StartChoice.IsChecked == true)
            {
                if (Start != -1)
                {
                    btns[Start].Background = EmptyBrush.Clone();
                }
                Fields[(int)button.Tag / n][(int)button.Tag % n] = 3000;
                button.Background = StartBrush.Clone();
                Start = (int) button.Tag;
            }
            if (StopChoice.IsChecked == true)
            {
                if (Stop != -1)
                {
                    btns[Stop].Background = EmptyBrush.Clone();
                }
                Fields[(int)button.Tag / n][(int)button.Tag % n] = 3000;
                button.Background = StopBrush.Clone();
                Stop = (int)button.Tag;
            }

            button.Background.Opacity = 0.65;
        }
        public void Board_Enter(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            button.Background.Opacity = 0.65;
        }
        public void Board_Leave(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            button.Background.Opacity = 1.0;
        }

        public void Change_Size(object sender, RoutedEventArgs e)
        {
            int n = cmbSize.SelectedIndex;
            NewBoard(n+5);
        }
        public void Clear_Board(object sender, RoutedEventArgs e)
        {
            NewBoard(CheckerBoard.Rows);
        }

        private void RefreshColors(SolidColorBrush OldPath)
        {
            WallBrushSquare.Fill = WallBrush;
            EmptyBrushSquare.Fill = EmptyBrush;
            StartBrushSquare.Fill = StartBrush;
            StopBrushSquare.Fill = StopBrush;

            int n = CheckerBoard.Rows;

            for (int i = 0; i < n * n; i++)
            {
                if (Fields[i / n][i % n] == -1) btns[i].Background = WallBrush.Clone();
                else if (i == Stop) btns[i].Background = StopBrush.Clone();
                else if (i == Start) btns[i].Background = StartBrush.Clone();
                else if (Equals(((SolidColorBrush)btns[i].Background).Color, OldPath.Color)) btns[i].Background = PathBrush.Clone();
                else btns[i].Background = EmptyBrush.Clone();
            }
        }

        public void Run(object sender, RoutedEventArgs e)
        {
            if (Stop == -1)
            {
                MessageBox.Show("Stop isn't set");
                return;
            }
            if (Start == -1)
            {
                MessageBox.Show("Start isn't set");
                return;
            }

            int x, y;
            int n = CheckerBoard.Rows;

            int StartX = Start % n, StartY = Start / n;
            int StopX = Stop % n, StopY = Stop / n;

            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    if (Fields[i][j] != -1)
                    {
                        Fields[i][j] = 3000;
                        if(i*n + j != Start && i*n + j != Stop)
                            btns[i * n + j].Background = EmptyBrush.Clone();
                    }
                }

            Fields[StartY][StartX] = 0;


            Tuple<int, int> pair = new Tuple<int, int>(StartY, StartX);
            Queue<Tuple<int, int>> Q = new Queue<Tuple<int, int>>();

            Q.Enqueue(pair);

            while (Q.Count != 0)
            {
                pair = Q.Dequeue();
                y = pair.Item1;
                x = pair.Item2;
                if (y != 0)
                    if (Fields[y - 1][x] - Fields[y][x] > 1)
                    {
                        Fields[y - 1][x] = Fields[y][x] + 1;
                        Q.Enqueue(new Tuple<int, int>(y - 1, x));
                    }
                if (y != n - 1)
                    if (Fields[y + 1][x] - Fields[y][x] > 1)
                    {
                        Fields[y + 1][x] = Fields[y][x] + 1;
                        Q.Enqueue(new Tuple<int, int>(y + 1, x));
                    }
                if (x != 0)
                    if (Fields[y][x - 1] - Fields[y][x] > 1)
                    {
                        Fields[y][x - 1] = Fields[y][x] + 1;
                        Q.Enqueue(new Tuple<int, int>(y, x - 1));
                    }
                if (x != n - 1)
                    if (Fields[y][x + 1] - Fields[y][x] > 1)
                    {
                        Fields[y][x + 1] = Fields[y][x] + 1;
                        Q.Enqueue(new Tuple<int, int>(y, x + 1));
                    }
            }

            if (Fields[StopY][StopX] == 3000)
            {
                MessageBox.Show("There's no way!");
                return;
            }
                
            MessageBox.Show(Fields[StopY][StopX].ToString());

            int min;
            x = StopX;
            y = StopY;
            int choice;

            while(x != StartX || y != StartY)
            {
                min = 100000;
                choice = 0;

                if (y != 0)
                    if (Fields[y - 1][x] == Fields[y][x] - 1
                        && Math.Abs((StartX - x) * (StopY - y + 1) - (StartY - y + 1) * (StopX - x)) < min)
                    {
                        choice = 1;
                        min = Math.Abs((StartX - x) * (StopY - y + 1) - (StartY - y + 1) * (StopX - x));
                    }
                if (y != n - 1)
                    if (Fields[y + 1][x] == Fields[y][x] - 1
                        && Math.Abs((StartX - x) * (StopY - y - 1) - (StartY - y - 1) * (StopX - x)) < min)
                    {
                        min = Math.Abs((StartX - x) * (StopY - y - 1) - (StartY - y - 1) * (StopX - x));
                        choice = 2;
                    }
                if (x != 0)
                    if (Fields[y][x - 1] == Fields[y][x] - 1
                        && Math.Abs((StartX - x + 1) * (StopY - y) - (StartY - y) * (StopX - x + 1)) < min)
                    {
                        min = Math.Abs((StartX - x + 1) * (StopY - y) - (StartY - y) * (StopX - x + 1));
                        choice = 3;
                    }
                if (x != n - 1)
                    if (Fields[y][x + 1] == Fields[y][x] - 1
                        && Math.Abs((StartX - x - 1) * (StopY - y) - (StartY - y) * (StopX - x - 1)) < min)
                    {
                        min = Math.Abs((StartX - x - 1) * (StopY - y) - (StartY - y) * (StopX - x - 1));
                        choice = 4;
                    }

                if (choice == 1) y--;
                if (choice == 2) y++;
                if (choice == 3) x--;
                if (choice == 4) x++;

                if(y*n + x != Start) btns[y * n + x].Background = PathBrush.Clone();
            }
        }
        public void OpenSettings(object sender, RoutedEventArgs e)
        {
            Window1 win = new Window1();
            SolidColorBrush temp = PathBrush.Clone();
            win.Owner = this;
            win.ShowDialog();
            RefreshColors(temp);
        }
    }
}
