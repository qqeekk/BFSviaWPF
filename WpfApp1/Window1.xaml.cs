﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        //ComboBox Wall;
        //ComboBox Empty;
        //ComboBox Path;
        public new MainWindow Owner;
        public static int[] dfcolors = { 9, 46, 104, 61, 113 };
        public static int[] colors = { 9, 46, 104, 61, 113 };

        public Window1()
        {
            InitializeComponent();
            var items = typeof(Colors).GetProperties();
            Wall.ItemsSource = items;
            Wall.SelectedIndex = colors[0];
            Empty.ItemsSource = items;
            Empty.SelectedIndex = colors[1];
            Path.ItemsSource = items;
            Path.SelectedIndex = colors[2];
            Start.ItemsSource = items;
            Start.SelectedIndex = colors[3];
            Stop.ItemsSource = items;
            Stop.SelectedIndex = colors[4];
        }

        private void Refresh()
        {
            Owner.WallBrush = (new SolidColorBrush((Color)(Wall.SelectedItem as PropertyInfo).GetValue(null))).Clone();
            Owner.EmptyBrush = (new SolidColorBrush((Color)(Empty.SelectedItem as PropertyInfo).GetValue(null))).Clone();
            Owner.PathBrush = (new SolidColorBrush((Color)(Path.SelectedItem as PropertyInfo).GetValue(null))).Clone();
            Owner.StartBrush = (new SolidColorBrush((Color)(Start.SelectedItem as PropertyInfo).GetValue(null))).Clone();
            Owner.StopBrush = (new SolidColorBrush((Color)(Stop.SelectedItem as PropertyInfo).GetValue(null))).Clone();
        }
        public void Save(object sender, RoutedEventArgs e)
        {
            colors[0] = Wall.SelectedIndex;
            colors[1] = Empty.SelectedIndex;
            colors[2] = Path.SelectedIndex;
            colors[3] = Start.SelectedIndex;
            colors[4] = Stop.SelectedIndex;
            Refresh();
            MessageBox.Show("Colors are changed");
            Close();
        }

        public void Default(object sender, RoutedEventArgs e)
        {
            Wall.SelectedIndex = dfcolors[0];
            Empty.SelectedIndex = dfcolors[1];
            Path.SelectedIndex = dfcolors[2];
            Start.SelectedIndex = dfcolors[3];
            Stop.SelectedIndex = dfcolors[4];
            Refresh();
        }
    }
}
