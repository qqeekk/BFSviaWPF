# A simple WPF application that finds the shortest way out of the custom labyrinth. 
### About
It uses a BFS algorithm, an algorithm for searching tree or graph data structures. 
It starts at the tree root and non-recursively explores the neighbor nodes first, before moving to the next level neighbors.

![The path is highlighted](https://pp.userapi.com/c840025/v840025093/6a217/fMgIb5TojgI.jpg)

### Settings
Yo can change the size of the labyrinth-grid and colors for all labirynth components such as walls, start and finish points of the route.

![Settings](https://pp.userapi.com/c840025/v840025093/6a21f/Zpu_O84jIHQ.jpg)